'use strinct'
const passport = require('passport');
const express = require("express");
const boom = require('@hapi/boom');
const cookieParser = require('cookie-parser');
const axios = require('axios');
const cors = require('cors');
const notFoundHandler =require('./src/utils/middleware/notFoundHandler');

const { config } = require("./src/config");

const corsOptions = {
  origin: config.app.corsOrigin
}

const app = express();

// body parser
app.use(express.json());
app.use(cookieParser());

//Cors configuration
app.use(cors(corsOptions));

//Time
const THIRTY_DAYS_IN_SEC = 2592000;
const TWO_HOURS_IN_SEC = 7200;

//Basic
require('./src/utils/auth/strategies/basic');

// auth
app.post("/auth/sign-in", async function(req, res, next) {
  const { rememberMe } = req.body;

  passport.authenticate('basic', function(error, data){
    const { token, user } = data;
    try {
      if(error || !data){
        next(boom.unauthorized());
      }

      req.login(data, {session: false}, async function(error){
        if(error){
          next(error);
        }

        res.cookie("token", token, {
          httpOnly: !config.dev,
          secure: !config.dev,
          maxAge: rememberMe ? THIRTY_DAYS_IN_SEC : TWO_HOURS_IN_SEC
        });

        res.status(200).json(user);
      });


    } catch (error) {
      next(error);
    }

  })(req, res, next);
});


// Products
app.get("/products", async function(req, res, next) {
  try {
    const { token } = req.cookies;

    const { data, status } = await axios({
      url: `${config.api.url}/api/products`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'get'
    });

    if (status !== 201) {
      return next(boom.badImplementation());
    }

    res.status(201).json(data);
    
  } catch (error) {
    next(error);
  }
});

app.get("/products/:id", async function(req, res, next) {

});

app.put("/products/:id", async function(req, res, next) {

});

app.delete("/products/:id", async function(req, res, next) {

});

// Store
app.get("/stores", async function(req, res, next) {
  try {
    const { token } = req.cookies;

    const { data, status } = await axios({
      url: `${config.api.url}/api/stores`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'get'
    });

    if (status !== 201) {
      return next(boom.badImplementation());
    }

    res.status(201).json(data);
    
  } catch (error) {
    next(error);
  }
});

app.get("/stores/:id", async function(req, res, next) {

});

app.put("/stores/:id", async function(req, res, next) {

});

app.delete("/stores/:id", async function(req, res, next) {

});


// Users
app.get("/users", async function(req, res, next) {
  try {
    const { token } = req.cookies;

    const { data, status } = await axios({
      url: `${config.api.url}/api/stores`,
      headers: { Authorization: `Bearer ${token}` },
      method: 'get'
    });

    if (status !== 201) {
      return next(boom.badImplementation());
    }

    res.status(201).json(data);
    
  } catch (error) {
    next(error);
  }
});

app.get("/users/:id", async function(req, res, next) {

});

app.put("/users/:id", async function(req, res, next) {

});

app.delete("/users/:id", async function(req, res, next) {

});

app.use(notFoundHandler);

app.listen(config.app.port, function() {
  console.log(`Listening http://localhost:${config.app.port}`);
});