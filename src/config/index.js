require('dotenv').config();

const config = {
    dev: process.env.MODE_ENV !== 'produccion',
    app: {
        port: process.env.PORT || 8000,
        corsOrigin: process.env.APP_ORIGIN_CORS,
    },
    keys: {
        publicApiKeyToken: process.env.PUBLIC_API_KEY_TOKEN,
        adminApiKeyToken: process.env.ADMIN_API_KEY_TOKEN
    },
    api: {
        url: process.env.API_URL
    }

}

module.exports = {
    config
}